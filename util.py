"""
Filename: util.py
Description: common utility functions
"""

from flask import current_app, url_for
import logging


def get_loggers():
    """
    Retrieve all loggers

    Return:
    loggers -- list of existing loggers.
    """
    loggers = [logging.getLogger(name)
               for name in logging.root.manager.loggerDict]
    return loggers


def get_url_map_list(url_map):
    """
    Convert 'Map' object of url_map to list

    Argument:
    url_map -- 'Map' object of app's url_map.

    Return:
    url_map_list -- List of url_map passed in.
    """
    url_map_list = []

    for rule in url_map.iter_rules():
        if 'GET' in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            current_app.logger.debug(url + ' is mapped to ' + rule.endpoint)
            url_map_list.append((url, rule.endpoint))

    return url_map_list


def has_no_empty_params(rule):
    """
    Determine whether a rule has no empty parameter.

    Argument:
    rule -- each key in url_map

    Return:
    boolean -- True if rule passed in has no empty parameter,
               False otherwise.
    """
    defaults = rule.defaults if rule.defaults is not None else ()
    args = rule.arguments if rule.arguments is not None else ()

    return len(defaults) >= len(args)
