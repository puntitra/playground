"""
Filename: config.py
Description: config file
"""

from os import environ, path
from dotenv import load_dotenv


class BaseConfig:
    """ Base configuration """
    APP_FOLDER = path.abspath(path.dirname(__file__))
    LOG_FILENAME = 'playground.log'
    STATIC_FOLDER = 'static'
    TEMPLATE_FOLDER = 'templates'
    # SECRET_KEY = os.environ.get('SECRET_KEY')


# Note: DEBUG is overriden by FLASK_DEBUG and FLASK_ENV
# https://flask.palletsprojects.com/en/1.0.x/config/#DEBUG
class DevConfig(BaseConfig):
    LOG_LEVEL = 'debug'
    DEBUG = True
    TESTING = True


class ProdConfig(BaseConfig):
    LOG_LEVEL = 'error'
    DEBUG = False
    TESTING = False
