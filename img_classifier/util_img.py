from flask import current_app
from os import path
import numpy as np
from PIL import Image
from keras.preprocessing.image import load_img, img_to_array
from keras.applications import resnet50, inception_v3, densenet


def save_img(image, image_dir, thumbnail_size=(600, 300)):
    """
    Read input image, set thumbnail and save image to static dir.

    Arguments:
    image -- input image file.
    image_dir -- directory to save image
    thumbnail_size -- size of thumbnail in tuple format (px, px)

    Return:
    success -- False if fail, True otherwise.
    """

    try:
        fname = image.filename
        fpath = path.join(image_dir, fname)
        current_app.logger.debug('fpath = ' + fpath)

        current_app.logger.info("Opening image: " + str(image))
        img_pil = Image.open(image)

        current_app.logger.info("Creating thumbnail.")
        img_pil.thumbnail(thumbnail_size, Image.ANTIALIAS)

        current_app.logger.info("Saving image to " + fpath)
        img_pil.save(fpath)

        success = True
    except IOError as err:
        success = False
        current_app.logger.error('IOError occurred.' + repr(err))
    except TypeError as err:
        success = False
        current_app.logger.error('TypeError occurred.' + repr(err))
    except:
        success = False
    finally:
        return success


def convert_img(image, target_size=(224, 224)):
    """
    Convert image to a Numpy array.

    Arguments:
    image -- input image file.
    target_size -- target array dimension in tuple format.

    Return:
    image_array -- a Numpy array of shape target_size.
    """

    current_app.logger.info("Loading image.")
    image_array = load_img(image, target_size=target_size)
    current_app.logger.info("Converting image to image array.")
    image_array = img_to_array(image_array)
    image_array = np.expand_dims(image_array, axis=0)

    return image_array


def pc_img(image_array, model_name='resnet50'):
    """
    Preprocess and classify image using specified pre-trained
    model.

    Arguments:
    image_array -- a Numpy array of image.
    model_name -- pre-trained model. Options are resnet50 (default),
                inceptionv3, densenet201.

    Return:
    pred -- classification outcome.
    """

    options = ['resnet50', 'inceptionv3', 'densenet201']

    model_name = model_name.strip().lower()

    if model_name not in options:
        model_name = 'resnet50'

    # Each model has their own methods for preprocessing and
    # decoding predictions.
    if model_name == 'inceptionv3':
        preprocess_input = inception_v3.preprocess_input
        model = inception_v3.InceptionV3(weights='imagenet')
        decode_predictions = inception_v3.decode_predictions
    elif model_name == 'densenet201':
        preprocess_input = densenet.preprocess_input
        model = densenet.DenseNet201(weights='imagenet')
        decode_predictions = densenet.decode_predictions
    else:
        preprocess_input = resnet50.preprocess_input
        model = resnet50.ResNet50(weights='imagenet')
        decode_predictions = resnet50.decode_predictions

    # Preprocess image array
    current_app.logger.info("Preprocessing image array.")
    image = preprocess_input(image_array)
    # Predict image and decode prediction
    current_app.logger.info("Predicting image.")
    pred = model.predict(image)
    current_app.logger.info("Decoding prediction.")
    pred = decode_predictions(pred)[0][0][1].replace('_', ' ')

    return pred
