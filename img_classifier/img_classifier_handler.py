from flask import Blueprint, render_template
from flask import request
from os import path
from .util_img import save_img, convert_img, pc_img


# Blueprint Config
classify_img_bp = Blueprint('classify_img_blueprint',
                             __name__,
                             template_folder='templates',
                             static_folder='static')


@classify_img_bp.route('/', methods=['GET', 'POST'])
def classify_img():

    title = 'Image Classifier'

    if request.method == 'POST':

        image = request.files['input_file']
        fname = image.filename
        img_folder = path.join(classify_img_bp.static_folder,
                               'img')
        model_name = request.form.get('model_name')

        if save_img(image, img_folder):
            image_array = convert_img(image)
            prediction = pc_img(image_array,
                                model_name=model_name)

            return render_template('classify_img.html',
                                   title=title,
                                   image_path=fname,
                                   prediction=prediction)
        else:
            return render_template('classify_img.html',
                                   title=title,
                                   image_path='upload.jpg')

    return render_template('classify_img.html',
                           title=title,
                           image_path='upload.jpg')
