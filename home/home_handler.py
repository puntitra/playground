from flask import Blueprint, render_template
from flask import current_app

# Blueprint Config
home_bp = Blueprint('home_blueprint',
                    __name__,
                    template_folder='templates',
                    static_folder='static')


@home_bp.route('/', methods=['GET'])
def home():

    description = 'Homepage of Playground'

    return render_template('home.html',
                           title='Home to Playground',
                           description=description)
