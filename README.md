[TOC]

# Playground: An image classifier with Flask

This repository offers a web application for classifing images using three pre-trained convolutional neural network (CNN) architechtures, namely resnet, inception, and densenet.

[//]: # "Image Classifier This repository offers ... This is an educational repository to learn Flask with Blueprints, jinja, brush up image processing, and play with pre-trained models using TensorFlow via Keras."

##Demo

![playground demo](docs/image_classifier.gif)

## Libraries

The main libraries used are Flask, Keras, TensorFlow, Pillow, and Jinja.

## Features
* Structured with Flask Blueprints for scalability
* Custom logging and error handling 
* Requirement files are available in both conda and pip formats
* Frontend rendering using Jinja.
* Development and production configurations
* A bonus Blueprint that include:
    * A mapping between each blueprint endpoint and route handler 
    * Configuration variables and values
    * List of all loggers being used (i.e. PIL, tensorflow, flask, urllib3) and their levels

## Setup

Clone this repository.

```bash
git clone https://puntitra@bitbucket.org/puntitra/playground.git

```

Change working directory to playground.
```bash
cd playground
```

Install packages. *Note:* Python3 is required. Also, using virtual environment is highly recommended. 
```bash
python3 -m pip install requirements.txt
```

*Note:* environment.yml is also available if conda is preferred. Enter `conda env create -f environment.yml`
