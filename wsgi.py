"""
Filename: wsgi.py
Description: Application's entry point
"""

from os import environ
from dotenv import load_dotenv
from . import create_app
from .config import DevConfig, ProdConfig


if environ.get('FLASK_ENV') == 'development':
    app = create_app(config=DevConfig)
else:
    app = create_app(config=ProdConfig)


if __name__ == "__main__":
    app.run()
