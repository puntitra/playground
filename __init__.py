"""
Filename: __init__.py
Description: Flask creation and registering blueprints
"""

from os import path
from flask import Flask
from .config import DevConfig, ProdConfig
from .logging import config_logger


def create_app(config=ProdConfig):
    """
    Create Flask application.

    Argument:
    config -- config object (optional)

    Return:
    app -- Flask application
    """

    # Initialize application.
    app = Flask(__name__)

    # Load configuration from config.DevConfig
    app.config.from_object(config)

    # Setup logger
    log_level = app.config['LOG_LEVEL']
    log_filename = app.config['LOG_FILENAME']
    logger = config_logger(level=log_level,
                           log_filename=log_filename)

    #### Global libraries (if any) ####
    ###################################

    with app.app_context():

        # Import error handlers. Note: it uses the current
        # application object.
        from . import errors

        ######## Initialize plugins #########
        #####################################

        ### Import modules for Blueprints ###
        logger.info('Importing modules for Blueprints')
        from .sanity_check import sanity_check_handler
        from .home import home_handler
        from .img_classifier import img_classifier_handler
        #####################################

        ######## Register Blueprints ########
        logger.info('Registering Blueprint for sanity_check')
        app.register_blueprint(sanity_check_handler.sanity_check_bp)

        logger.info('Registering Blueprint for home')
        app.register_blueprint(home_handler.home_bp,
                               url_prefix='/home')

        logger.info('Registering Blueprint for img_classifier')
        app.register_blueprint(img_classifier_handler.classify_img_bp,
                               url_prefix='/img_classifier')
        #####################################

        return app
