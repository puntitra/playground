from flask import Blueprint, render_template
from flask import current_app, request
from playground.util import get_url_map_list, get_loggers


# Blueprint Config
sanity_check_bp = Blueprint('sanity_check_blueprint',
                            __name__,
                            template_folder='templates',
                            static_folder='static')


@sanity_check_bp.route('/', methods=['GET'])
def sanity_check():

    description = 'This is left here only for learning purpose. Unregister this Blueprint!'
    url_map_list = get_url_map_list(current_app.url_map)
    logger_list = get_loggers()

    return render_template('sanity_check.html',
                           title='Sanity Check',
                           description=description,
                           url_map_list=url_map_list,
                           logger_list=logger_list,
                           config_dict=current_app.config)
