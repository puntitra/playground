"""
Filename: errors.py
Description: Error handling functions
"""

from flask import make_response, render_template
from flask import current_app
from werkzeug.exceptions import HTTPException, InternalServerError

# TODO: Custom Exception class
# Return only REST error response (if API module(s) is added)
# Specifically handle non-HTTPExceptions
# (idea) Specific error handling for each Blueprint?


@current_app.errorhandler(HTTPException)
def handle_http_exception(e):

    current_app.logger.error("Handling HTTPException -- Code " + str(e.code))

    return make_response(render_template('error.html',
                                         e_code=e.code,
                                         e_name=e.name,
                                         e_desc=e.description),
                         e.code)


@current_app.errorhandler(Exception)
def handle_all_exception(e):

    # Pass through HTTP errors
    if isinstance(e, HTTPException):
        return e
    else:
        current_app.logger.error("Handling Non-HTTPException")
        e = InternalServerError()

    return make_response(render_template('error.html',
                                         e_code=e.code,
                                         e_name=e.name,
                                         e_desc=e.description),
                         e.code)
